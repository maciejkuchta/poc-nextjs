import {createContext} from "react";

export const AppContext = createContext({
    sidebarOpen: true,
    toggleSidebarOpen: () => {}
});