import axios from './axios';

export function getHomePageData() {
    return axios.get(process.env.API_URL+'/home-page/data').then(response => {
        return response.data;
    });
}