import axios from './axios';

import { IFormData } from "../ts_common/IContactPage";

export function postNewMessage(data: IFormData) {
    return axios.post(process.env.API_URL+'/contact-messages', {data}).then(response => {
        return response.data;
    });
}