import axios from './axios';


export function getMessages() {
    return axios.get(process.env.API_URL+'/contact-messages?sort[0]=createdAt%3Adesc').then(response => {
        return response.data.data;
    });
}