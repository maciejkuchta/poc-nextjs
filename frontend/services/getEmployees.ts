import axios from './axios';


export function getAllEmployees() {
    return axios.get(process.env.API_URL+'/employees?populate=subordinates', {responseType: 'json'}).then(response => {
        console.log(response.data.data);
        return response.data.data;
    });
}