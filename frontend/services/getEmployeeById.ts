import axios from './axios';

export function getEmployeeById(id: number | string) {
    return axios.get(process.env.API_URL+`/employees/${id}?populate=*`).then(response => {
        return response.data.data;
    });
}