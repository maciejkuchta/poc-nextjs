import axios from './axios';

export function getOfficeStructure() {
    return axios.get(process.env.API_URL+'/employees/office-structure').then(response => {
        return response.data;
    });
}