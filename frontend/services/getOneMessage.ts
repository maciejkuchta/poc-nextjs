import axios from './axios';


export function getOneMessage(id: string | number) {
    return axios.get(process.env.API_URL+'/contact-messages/'+id).then(response => {
        return response.data.data;
    });
}