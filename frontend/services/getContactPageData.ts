import axios from './axios';

export function getContactPageData() {
    return axios.get(process.env.API_URL+'/contact-page/data').then(response => {
        return response.data;
    });
}