import React from 'react';
import Head from "next/head";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

import Layout from "../components/Layout/Layout";

function PageNotFound() {
    return (
        <Layout>
            <Head>
                <title>
                    404 - nie znaleziono strony
                </title>
            </Head>
            <Box height="100%" width="100%" display="flex" alignItems="center" justifyContent="center" flexDirection="column">
                <Typography fontWeight={600} fontSize="25px">
                    Wskazana strona nie istnieje
                </Typography>
                <Typography fontSize="18px">
                    Skorzystaj z nawigacji, aby wrócić do zawartości aplikacji
                </Typography>
            </Box>
        </Layout>
    );
}

export default PageNotFound;