import React, {useEffect} from 'react';
import {useQuery} from "react-query";
import Box from "@mui/material/Box";

import Layout from "../../components/Layout/Layout";
import {getMessages} from "../../services/getMessages";
import {IMessage} from "../../ts_common/IMessage";
import MessageBox from "../../components/MessageBox";
import Typography from "@mui/material/Typography";
import {Divider} from "@mui/material";
import Head from "next/head";

interface IProps {
    setLoading: (isLoading: boolean) => void
}

function MessagesPage(props: IProps) {
    const { isLoading, isError, data } = useQuery("getMessages", getMessages);

    useEffect(() => {
        props.setLoading(isLoading);
    }, [isLoading])

    return (
        <Layout>
            <Box display="flex" flexDirection="column" alignItems="center">
                <Head>
                    <title>
                        Wiadomości
                    </title>
                </Head>
                <Typography variant='h1' pb='30px'>
                    Wiadomości
                </Typography>
                <Divider sx={{mb: '30px', width: '100%'}} />
                <Box display="flex" flexDirection="column" width="100%" alignItems="center">
                    {data && data.map((message: IMessage) => {
                        return <MessageBox key={"message-"+message.id} messageData={message} />
                    })}
                </Box>
            </Box>
        </Layout>
    );
}

export default MessagesPage;