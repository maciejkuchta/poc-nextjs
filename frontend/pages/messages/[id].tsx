import React, {useEffect} from 'react';
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";

import {IMessage} from "../../ts_common/IMessage";
import {format} from "date-fns";
import {useQuery} from "react-query";
import {getOneMessage} from "../../services/getOneMessage";
import {useRouter} from "next/router";
import Layout from "../../components/Layout/Layout";
import DetailsLabeledContent from "../../components/DetailsLabeledContent";
import Typography from "@mui/material/Typography";


interface IProps {
    setLoading: (isLoading: boolean) => void
}

function MessageDetails(props: IProps) {
    const router = useRouter();

    const {isLoading, isError, data} = useQuery("getOneMessage", () => typeof router.query.id === "string" && getOneMessage(router.query.id))

    useEffect(() => {
        props.setLoading(isLoading);
    }, [isLoading])

    if (!data) return null;

    return(
        <Layout>
            <Paper elevation={3} sx={{p: "25px 50px", borderRadius: '12px'}}>
                <Grid container spacing={4}>
                    <Grid item xs={12} container justifyContent="flex-end">
                        <Typography fontSize="14px" fontWeight={300}>
                            {format(new Date(data.attributes.createdAt), "dd/MM/yyyy")}
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <DetailsLabeledContent label="Imię i nazwisko" content={data.attributes.name} />
                    </Grid>
                    <Grid item xs={6}>
                        <DetailsLabeledContent label="E-mail" content={data.attributes.email} />
                    </Grid>
                    <Grid item xs={6}>
                        <DetailsLabeledContent label="Numer telefonu" content={data.attributes.phoneNumber} />
                    </Grid>
                    <Grid item xs={12}>
                        <DetailsLabeledContent label="Temat" content={data.attributes.topic} />
                    </Grid>
                    <Grid item xs={12}>
                        <DetailsLabeledContent label="Treść" content={data.attributes.content} />
                    </Grid>
                </Grid>
            </Paper>
        </Layout>
)
;
}

export default MessageDetails;