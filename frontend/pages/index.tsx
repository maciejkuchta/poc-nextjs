import Layout from "../components/Layout/Layout";
import Box from "@mui/material/Box";
import {getHomePageData} from "../services/getHomePageData";
import { IHomePageData } from "../ts_common/IHomePage";
import Typography from "@mui/material/Typography";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemIcon from "@mui/material/ListItemIcon";
import {ChevronRight} from "@mui/icons-material";
import TechnologyBox from "../components/TechnologyBox";
import Head from "next/head";
import React from "react";

const Home = (props: {homePageData: IHomePageData}) => {
  return (
    <Layout>
        <Head>
          <title>
            Strona startowa
          </title>
        </Head>
        <Box borderRadius='10px 10px 0 0' p='50px' sx={{backgroundImage: `url(${process.env.HOST+props.homePageData.main_banner_image.url})`, backgroundSize: {xs: 'auto 650px', sm: 'auto 450px', lg: 'auto 350px'}, backgroundRepeat: 'no-repeat', backgroundPosition:'top right'}}>
          <Typography variant='h1' sx={{color: theme => theme.palette.primary.main, fontWeight: 400, fontSize: '48px'}}>
            {props.homePageData.title}
          </Typography>
          <Box p='30px 0'>
            <Typography fontWeight={400}>
              {props.homePageData.description.content}
            </Typography>
            <List >
              {props.homePageData.description.text_records.map(record => {
                return <ListItem key={'desc-txt-record-'+record.id}>
                  <ListItemIcon>
                    <ChevronRight color='secondary' />
                  </ListItemIcon>
                  <ListItemText primary={record.content} />
                </ListItem>
              })}
            </List>
          </Box>
          <Box p="50px 0" display='flex' flexDirection='column' alignItems='center'>
            <Typography fontSize='35px' sx={{color: theme => theme.palette.primary.main}}>
              {props.homePageData.section_subtitle}
            </Typography>
            <Box display='flex' flexWrap='wrap' justifyContent="center">
              {props.homePageData.technologies.map(tech => {
                return <TechnologyBox key={'tech-box-'+tech.id} technology={tech} />
              })}
            </Box>
          </Box>
        </Box>
    </Layout>
  )
}

export async function getStaticProps() {
  const homePageData = await getHomePageData();

  return {
    props: {
      homePageData
    },
  }
}

export default Home
