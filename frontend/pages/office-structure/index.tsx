import React from 'react';
import { ChevronRight, ExpandMore, BrightnessHigh, BrightnessLow} from "@mui/icons-material";
import TreeView from "@mui/lab/TreeView";
import TreeItem from "@mui/lab/TreeItem";
import Avatar from "@mui/material/Avatar";
import Layout from "../../components/Layout/Layout";
import {getOfficeStructure} from "../../services/getOfficeStructure";
import { IEmployeeFlat } from "../../ts_common/IEmployee";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {styled} from "@mui/styles";
import {Divider} from "@mui/material";
import Paper from "@mui/material/Paper";
import Head from "next/head";


interface ITreeItemLabelProps {
    primaryText: string,
    secondaryText: string,
    avatar: string | null,
    isChairperson: boolean
}


const StyledTreeItem = styled(TreeItem)(() => ({
    '& .MuiTreeItem-content': {
        paddingLeft: '30px',
        borderRadius: '10px',
    }
}))

function TreeItemLabel({primaryText, secondaryText, avatar, isChairperson}: ITreeItemLabelProps) {{
    return  <Box sx={{ display: 'flex', alignItems: 'center', p: '20px' }}>
        <Avatar src={avatar ? process.env.HOST + avatar : undefined} alt={primaryText + ' avatar'} sx={{ mr: '20px', width: '60px', height: '60px'}} />
        <Typography sx={{ flexGrow: 1, fontSize: '24px' }}>
            {primaryText}
        </Typography>
        <Typography color="inherit" fontStyle='italic' fontSize='20px'>
            {secondaryText}
        </Typography>
    </Box>
}}


function OfficeStructure({officeStructure}: {officeStructure: IEmployeeFlat[]}) {

    function renderTreeItems(employee: IEmployeeFlat) {
        if (employee.subordinates && employee.subordinates.length === 0) {
            return <StyledTreeItem key={employee.id} nodeId={employee.id.toString()} label={<TreeItemLabel isChairperson={employee.isChairperson} avatar={employee.avatar ? employee.avatar.url : null} primaryText={employee.firstName + ' ' + employee.lastName} secondaryText={employee.position}/> } />
        }
        return <StyledTreeItem
            collapseIcon={employee.isChairperson ? <BrightnessLow sx={{width: '40px', height: '40px', color: theme => theme.palette.primary.main}}  /> : undefined}
            expandIcon={employee.isChairperson ? <BrightnessHigh sx={{width: '40px', height: '40px', color: theme => theme.palette.primary.main}}  /> : undefined}
            key={employee.id}
            nodeId={employee.id.toString()}
            label={
            <TreeItemLabel
                isChairperson={employee.isChairperson}
                avatar={employee.avatar ? employee.avatar.url : null}
                primaryText={employee.firstName + ' ' + employee.lastName}
                secondaryText={employee.position}
            />}
        >
            {employee.subordinates && employee.subordinates.map(subordinate => renderTreeItems(subordinate))}
        </StyledTreeItem>

    }

    return (
        <Layout>
            <Head>
                <title>
                    Xentivo - struktura
                </title>
            </Head>
            <Box display='flex' flexDirection='column' alignItems='center' width='100%'>
                    <Typography variant='h1' pb='30px'>
                        Struktura firmy
                    </Typography>
                    <Divider sx={{mb: '30px', width: '100%'}} />
                    <Paper elevation={3} sx={{p: '40px', width: '100%', height: '100%', borderRadius: '10px'}}>
                    <TreeView
                        aria-label="office-structure-tree"
                        sx={{minWidth: '80%'}}
                        defaultCollapseIcon={<ExpandMore sx={{width: '50px', height: '50px', color: theme => theme.palette.primary.dark}} />}
                        defaultExpandIcon={<ChevronRight  sx={{width: '50px', height: '50px', color: theme => theme.palette.primary.main}} />}
                    >
                        {
                             officeStructure.map((employee: IEmployeeFlat) => renderTreeItems(employee))
                        }
                    </TreeView>
                    </Paper>
            </Box>
        </Layout>
    );
}

export async function getStaticProps() {
    const officeStructure = await getOfficeStructure();

    return {
        props: {
            officeStructure
        },
    }
}


export default OfficeStructure;