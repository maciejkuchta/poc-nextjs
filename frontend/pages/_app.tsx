import '../styles/globals.css'
import type { AppProps } from 'next/app'
import {QueryClient, QueryClientProvider} from "react-query";
import Head from "next/head";
import {useEffect, useState} from "react";
import {useRouter} from "next/router";
import CircularProgress from "@mui/material/CircularProgress";
import Backdrop from "@mui/material/Backdrop";
import {ThemeProvider, createTheme} from "@mui/material/styles";
import { AppContext } from "../AppContext/AppContext";

function MyApp({ Component, pageProps }: AppProps) {
  const queryClient = new QueryClient()
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [sidebarOpen, setSidebarOpen] = useState(true);

  const theme = createTheme({
    palette: {
      primary: {
        main: '#fba44b',
        light: '#ffbe7b',
        dark: '#ed8317'
      },
      secondary: {
        main: '#5697FF',
        light: '#79B1FDFF',
        dark: '#216cde'
      }
    },
    typography: {
      h1: {
        fontSize: '42px',
        fontWeight: 500
      },
      body1: {
        fontSize: '18px',
      },
      caption: {
        fontSize: '14px'
      }
    }
  });

  useEffect(() => {
    const handleStart = () =>  setLoading(true);
    const handleComplete = () =>  setLoading(false);

    router.events.on('routeChangeStart', handleStart)
    router.events.on('routeChangeComplete', handleComplete)
    router.events.on('routeChangeError', handleComplete)

    return () => {
      router.events.off('routeChangeStart', handleStart)
      router.events.off('routeChangeComplete', handleComplete)
      router.events.off('routeChangeError', handleComplete)
    }
  }, [])

  const contextInitValue = {
    sidebarOpen,
    toggleSidebarOpen: () => setSidebarOpen(prevState => !prevState)
  }

  return <>
    <Head>
      <title>NextJS Tutorial</title>
      <link rel="shortcut icon" href="/favicon.ico" />
    </Head>
    <main>
          <Backdrop open={loading} sx={{display: 'flex', justifyContent: 'center', alignItems: 'center', zIndex: 2000}}>
            <CircularProgress size={120} />
          </Backdrop>
        <QueryClientProvider client={queryClient}>
          <AppContext.Provider value={contextInitValue}>
            <ThemeProvider theme={theme}>
                <Component {...pageProps} setLoading={setLoading} />
            </ThemeProvider>
          </AppContext.Provider>
      </QueryClientProvider>
    </main>
  </>
}

export default MyApp
