import React, {FormEvent, useEffect, useState} from 'react';
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Grow from "@mui/material/Grow";
import Button from "@mui/material/Button";
import {useMutation} from "react-query";
import Head from "next/head";

import Layout from "../../components/Layout/Layout";
import LabeledTextField from "../../components/LabeledTextField";
import { IEvent, IFormData, IContactPageData, IFormValidation } from "../../ts_common/IContactPage";
import {postNewMessage} from "../../services/postNewMessage";
import {getContactPageData} from "../../services/getContactPageData";
import CompanyInfoBox from "../../components/CompanyInfoBox";



const initFormData: IFormData = {
    name: "",
    email: "",
    phoneNumber: "",
    topic: "",
    content: ""
}

const initFormTouched = {
    name: false,
    email: false,
    phoneNumber: false,
    topic: false,
    content: false
}

const validation: IFormValidation = {
    name: {
        required: true
    },
    email: {
        regex: {
            value: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            errorMsg: "Nieprawidłowy format adresu e-mail"
        },
        required: true
    },
    phoneNumber: {
        regex: {
            value: /^\d{9}$/,
            errorMsg: "Nieprawidłowy format numeru telefonu"
        }
    },
    topic: {
        required: true
    },
    content: {
        required: true
    }
}

interface IProps {
    contactPageData: IContactPageData,
    setLoading: (isLoading: boolean) => void
}

function Contact(props: IProps) {
    const [formData, setFormData] = useState(initFormData);
    const [errors, setErrors] = useState(initFormData);
    const [touched, setTouched] = useState(initFormTouched);
    const mutation = useMutation((data: IFormData) => postNewMessage(data).then(() => {
        setTouched(initFormTouched);
        setErrors(initFormData);
        setFormData(initFormData);
    }))

    useEffect(() => {
        props.setLoading(mutation.isLoading);
    }, [mutation.isLoading])

    useEffect(() => {
        validate();
    }, [formData])

    function handleFormChange(e: IEvent) {
        setTouched(prevState => ({...prevState, [e.target.name]: true}));
        setFormData(prevState => ({...prevState, [e.target.name]: e.target.value}));
    }

    function handlePostMessage(e: FormEvent) {
        e.preventDefault();
        mutation.mutate(formData);
    }

    function validate() {
        for (let key in validation) {

            if (validation[key].required && formData[key] === "") {
                setErrors(prevState => ({...prevState, [key]: "Pole jest wymagane"}));
            } else if (!validation[key]?.regex?.value.test(formData[key])) {
                setErrors(prevState => ({...prevState, [key]: validation[key]?.regex?.errorMsg || ""}));
            } else if (errors[key]) {
                setErrors(prevState => ({...prevState, [key]: ""}));
            }
        }
    }

    function checkIfFormDataValid() {
        for (let key in errors) {

            if (errors[key]) {
                return false;
            }
        }
        return true;
    }

    return (
        <Layout>
            <Head>
                <title>Skontaktuj się z nami!</title>
            </Head>
            <form style={{display: 'flex', justifyContent: 'center'}} onSubmit={handlePostMessage}>
                <Grid container spacing={3}>
                    <Grid item xs={12} sx={{textAlign: "center"}}>
                        <Typography variant='h1' pb='60px'>
                            {props.contactPageData.title}
                        </Typography>
                    </Grid>
                    <Grid container item xs={12} lg={3} justifyContent="center">
                        <CompanyInfoBox company={props.contactPageData.company} socialMediaLinks={props.contactPageData.social_media_links} />
                    </Grid>
                    <Grid container item xs={12} lg={9}>
                        <Grid item container spacing={3}>
                    <Grid container item spacing={{xs: 0, md: 4}} rowSpacing={{xs: 3, md: 0}}>
                        <Grid item xs={12} md={4}>
                        <LabeledTextField touched={touched.name} errorMsg={errors.name} label="Imię i nazwisko" name="name" value={formData.name} handleChange={handleFormChange} required />
                        </Grid>
                        <Grid item xs={12} md={4}>
                        <LabeledTextField touched={touched.email} errorMsg={errors.email} label="E-mail" name="email" value={formData.email} handleChange={handleFormChange} required />
                        </Grid>
                        <Grid item xs={12} md={4}>
                        <LabeledTextField touched={touched.phoneNumber} errorMsg={errors.phoneNumber} label="Numer telefonu" name="phoneNumber" value={formData.phoneNumber} handleChange={handleFormChange} />
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <LabeledTextField touched={touched.topic} errorMsg={errors.topic} label="Temat" name="topic" value={formData.topic} handleChange={handleFormChange} required />
                    </Grid>
                    <Grid item xs={12} >
                        <LabeledTextField touched={touched.content} errorMsg={errors.content} label="Treść" name="content" value={formData.content} handleChange={handleFormChange} multiline rows={5} required />
                    </Grid>
                    <Grid container item xs={12} justifyContent='flex-end'>
                        <Button disabled={!checkIfFormDataValid()} color='secondary' variant='contained' type='submit' sx={{color: '#fff'}}>
                            wyślij
                        </Button>
                    </Grid>
                        <Grid item xs={12}>
                            <Grow in={mutation.isError || mutation.isSuccess}>
                            <Typography textAlign='center' sx={{color: theme => mutation.isError ? theme.palette.error.main : theme.palette.success.main}}>
                                {mutation.isError ? "Wystąpił błąd podczas wysyłania danych" : "Wiadomość została pomyślnie wysłana"}
                            </Typography>
                            </Grow>
                        </Grid>
                    </Grid>
                    </Grid>
                </Grid>
            </form>
        </Layout>
    );
}

export async function getStaticProps() {
    const contactPageData = await getContactPageData();

    return {
        props: {
            contactPageData
        },
    }
}

export default Contact;