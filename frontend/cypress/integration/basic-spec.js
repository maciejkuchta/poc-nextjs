describe('The Home Page', () => {
    it('successfully loads', () => {
        cy.visit('http://localhost:3000') // change URL to match your dev URL
    })
    it('navigates to office structure', () => {
        cy.intercept('/employees/office-structure', {fixture: 'office-structure.json'}).as('office-structure');
        cy.get('[data-cy=sidebar-office-structure-btn]').click();
        // cy.wait('@office-structure').then((interception) => {
        //     console.log(interception);
        // })
    })
})