import React from 'react';
import {IMessage} from "../ts_common/IMessage";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import { format } from "date-fns"
import Divider from "@mui/material/Divider";
import {useRouter} from "next/router";
import Box from "@mui/material/Box";


function MessageBox(props: {messageData: IMessage}) {
    const router = useRouter();

    function handleChangePage() {
        router.push("/messages/"+props.messageData.id).then(() => {});
    }

    return (
        <Paper className="message-box" onClick={handleChangePage} elevation={3} sx={{m: "20px 0", p: "15px", width: "80%", borderRadius: '12px', cursor: "pointer", transition: ""}}>
            <style jsx>{`
               .message-box {
                  margin: 20px 0;
                  padding: 15px;
                  width: 80%;
                  border-radius: 12px;
                  cursor: pointer;
                  transform: scale(1);
                  transition: all 225ms !important;
               }
               .message-box:hover {
                  transform: scale(1.2);
               }
              `}</style>
            <Grid container>
                <Grid container item xs={12} justifyContent="space-between">
                    <Box display="flex">
                        <Typography fontWeight={600} pr="15px">
                            Temat:
                        </Typography>
                        <Typography>
                            {props.messageData.attributes.topic}
                        </Typography>
                    </Box>
                    <Box display="flex">
                        <Typography fontWeight={600} pr="15px">
                            Data:
                        </Typography>
                        <Typography>
                            {format(new Date(props.messageData.attributes.createdAt), "dd/MM/yyyy")}
                        </Typography>
                    </Box>
                    <Grid item xs={12}>
                        <Divider sx={{m: "15px 0"}} />
                    </Grid>
                    <Grid item xs={12} sx={{p: "0 20px 20px 20px"}}>
                        <Typography>
                            {props.messageData.attributes.content}
                        </Typography>
                    </Grid>
                </Grid>
            </Grid>
        </Paper>
    );
}

export default MessageBox;