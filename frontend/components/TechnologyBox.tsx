import React from 'react';
import { ITechnology } from "../ts_common/IHomePage";
import Box from "@mui/material/Box";
import Image from "next/image";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemIcon from "@mui/material/ListItemIcon";
import {ChevronRight} from "@mui/icons-material";

function TechnologyBox(props: {technology: ITechnology}) {
    return (
        <Box display='flex' flexDirection='column' maxWidth='400px' alignItems='center'>
            <Image src={process.env.HOST + props.technology.image.url} alt={props.technology.name + ' obraz'} width={200} height={200} objectFit='scale-down' />
            <List sx={{display: 'flex', flexDirection: 'column', alignSelf: 'flex-start'}}>
                {
                    props.technology.text_records.map(record => {
                        return <ListItem key={props.technology.name+'-tech-record-'+record.id}>
                            <ListItemIcon>
                                <ChevronRight color='secondary' />
                            </ListItemIcon>
                            <ListItemText primary={record.content} />
                        </ListItem>
                    })
                }
            </List>
        </Box>
    );
}

export default TechnologyBox;