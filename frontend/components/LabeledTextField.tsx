import React from 'react';
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";

import { IEvent } from "../ts_common/IContactPage";

interface IProps {
    name: string,
    value: string,
    label: string,
    multiline?: boolean,
    rows?: number,
    required?: boolean,
    handleChange: (e: IEvent) => void,
    errorMsg?: string,
    touched?: boolean
}


function LabeledTextField(props: IProps) {
    return (
        <Box display='flex' flexDirection='column' alignItems='flex-start'>
            <Typography pb='15px'>
                {props.label}
                {props.required &&
                    <Typography component="span" color="darkred" fontSize="18px">
                        *
                    </Typography>
                }
            </Typography>
            <TextField error={!!props.errorMsg && props.touched} helperText={props.touched ? props.errorMsg : ""} fullWidth multiline={props.multiline} rows={props.rows}  name={props.name} onChange={props.handleChange} value={props.value} />
        </Box>
    );
}

export default LabeledTextField;