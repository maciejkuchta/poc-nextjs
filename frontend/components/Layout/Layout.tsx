import React from 'react';
import Box from "@mui/material/Box";

import Header from "./components/Header";
import Footer from "./components/Footer";
import Sidebar from "./components/Sidebar";
import { AppContext } from "../../AppContext/AppContext";

function Layout({children}: {children: any }) {
        return (
            <AppContext.Consumer>
                {({sidebarOpen}) => (
            <Box display='flex' flexDirection='column' height='100%'>
                <Header />
                <Box m={{xs: '10px', md: '50px'}} sx={{transition: 'padding 225ms'}} pt='64px' pl={sidebarOpen ? '300px' : 0} display='flex' flexGrow={1} justifyContent='center'>
                    {children}
                </Box>
                    <Sidebar />
                <Footer />
            </Box>
                )}
            </AppContext.Consumer>
        );
};


export default Layout;