import React from 'react';
import Drawer from "@mui/material/Drawer";
import Divider from "@mui/material/Divider";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Link from "next/link";
import { People, Home, Mail } from "@mui/icons-material";
import { useRouter } from 'next/router';
import Image from "next/image";

import { AppContext } from "../../../AppContext/AppContext";

const listItems = [
    {
        text: "Home",
        href: "/",
        Icon: Home
    },
    {
        text: "Struktura biura",
        href: "/office-structure",
        Icon: People
    }
];

function Sidebar() {
    const router = useRouter()
    return (
        <AppContext.Consumer>
            {({sidebarOpen}) => (
        <Drawer
            open={sidebarOpen}
            sx={{
                width: 300,
                flexShrink: 0,
                '& .MuiDrawer-paper': {
                    width: 300,
                    boxSizing: 'border-box',
                },
            }}
            variant="persistent"
            anchor="left"
        >
            <Toolbar>
                <Image src='/images/xentivo.png' objectFit='contain'   width={227} height={37} alt='Xentivo logo' />
            </Toolbar>
            <Divider />
            <List>
                    <Link href="/" passHref>
                        <ListItem data-cy="sidebar-home-page-btn" button sx={router.pathname === "/" ? {color: theme => theme.palette.secondary.main, fontWeight: 500 } : undefined}>
                            <ListItemIcon sx={{color: 'inherit'}}>
                                <Home sx={{color: 'inherit'}} />
                            </ListItemIcon>
                            <ListItemText primary="Home" />
                        </ListItem>
                    </Link>
                    <Link href="/office-structure" passHref>
                        <ListItem data-cy="sidebar-office-structure-btn" button sx={router.pathname === "/office-structure" ? {color: theme => theme.palette.secondary.main, fontWeight: 600 } : undefined}>
                            <ListItemIcon sx={{color: 'inherit'}}>
                                <People sx={{color: 'inherit'}} />
                            </ListItemIcon>
                            <ListItemText primary="Struktura firmy" sx={{fontWeight: 'inherit'}} />
                        </ListItem>
                    </Link>
                <Link href="/messages" passHref>
                    <ListItem data-cy="sidebar-messages-btn" button sx={router.pathname === "/messages" ? {color: theme => theme.palette.secondary.main, fontWeight: 600 } : undefined}>
                        <ListItemIcon sx={{color: 'inherit'}}>
                            <Mail sx={{color: 'inherit'}} />
                        </ListItemIcon>
                        <ListItemText primary="Wiadomości" sx={{fontWeight: 'inherit'}} />
                    </ListItem>
                </Link>
            </List>
        </Drawer>
            )}
        </AppContext.Consumer>
    );
}

export default Sidebar;