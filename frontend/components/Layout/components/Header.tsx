import React from 'react';
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import AppBar from "@mui/material/AppBar";
import IconButton from "@mui/material/IconButton";
import Button from "@mui/material/Button";
import { Menu, Mail } from "@mui/icons-material";

import { AppContext } from "../../../AppContext/AppContext";
import {useRouter} from "next/router";

function Header() {
    const router = useRouter();

    function handleRedirectToContact() {
        router.push("/contact").then(res => res);
    }

    return (
        <AppContext.Consumer>
            {({sidebarOpen, toggleSidebarOpen}) => (
        <Box>
            <AppBar elevation={4} position="fixed" sx={{transition: 'padding 225ms', pl: sidebarOpen ? '300px' : 0, bgcolor: '#fff'}}>
                <Toolbar>
                    <IconButton
                        data-cy="sidebar-toggle-btn"
                        onClick={toggleSidebarOpen}
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }}
                    >
                        <Menu />
                    </IconButton>
                    <Box flexGrow={1} overflow='hidden' />
                    <Button onClick={handleRedirectToContact} color="inherit" startIcon={<Mail color='primary' />}>Kontakt</Button>
                </Toolbar>
            </AppBar>
        </Box>
            )}
        </AppContext.Consumer>
    );
}

export default Header;