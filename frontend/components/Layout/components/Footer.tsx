import React from 'react';
import Paper from "@mui/material/Paper";
import { Diamond } from "@mui/icons-material"
import Typography from "@mui/material/Typography";

import { AppContext } from "../../../AppContext/AppContext";

function Footer() {
    return (
        <AppContext.Consumer>
            {({sidebarOpen}) => (
        <Paper elevation={4} component='footer' sx={{transition: 'padding 225ms', pl: sidebarOpen ? '300px' : 0, width: '100vw', minHeight: '75px', marginTop: 'auto', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
            <Diamond sx={{mr: '10px'}}/>
            <Typography>
                Test App
            </Typography>
        </Paper>
            )}
        </AppContext.Consumer>
    );
}

export default Footer;