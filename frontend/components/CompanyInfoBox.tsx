import React from 'react';
import Grid from "@mui/material/Grid";
import Link from "@mui/material/Link";
import {ICompany, ISocialMediaLink} from "../ts_common/IContactPage";
import SocialMediaLink from "./SocialMediaLink";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

interface IProps {
    company: ICompany,
    socialMediaLinks: ISocialMediaLink[]
}

function CompanyInfoBox(props: IProps) {
    return (
        <Box display="flex" flexDirection="column" width="150px" pb="20px" alignItems="flex-start">
            <Typography>
                {props.company.name}
            </Typography>
            <Typography>
                {props.company.street + " " + props.company.building_number}
            </Typography>
            <Typography>
                {props.company.postcode + " " + props.company.city}
            </Typography>
            <Typography>
                <Link href={`mailto: ${props.company.email}`}>
                    {props.company.email}
                </Link>
            </Typography>
            <Typography>
                {props.company.phoneNumber}
            </Typography>
            <Box display="flex" pt="20px" justifyContent="center" width="100%">
                {props.socialMediaLinks.map(link => <SocialMediaLink key={link.name}  {...link} />)}
            </Box>
        </Box>
    );
}

export default CompanyInfoBox;