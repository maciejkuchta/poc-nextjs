import React from 'react';
import Image from "next/image";
import Link from "@mui/material/Link";

import {ISocialMediaLink} from "../ts_common/IContactPage";

function SocialMediaLink(props: ISocialMediaLink) {
    return (
        <Link href={props.url} sx={{mx: '5px'}}>
            <Image src={process.env.HOST + props.logo.url} alt={props.name} width={32} height={32} />
        </Link>
    );
}

export default SocialMediaLink;