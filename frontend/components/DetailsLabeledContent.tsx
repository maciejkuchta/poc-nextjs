import React from 'react';
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";

interface IProps {
    label: string,
    content: string
}

function DetailsLabeledContent(props: IProps) {
    return (
        <Box display="flex" flexDirection="column">
            <Typography fontWeight={600} fontSize="16px">
                {props.label}
            </Typography>
            <Divider sx={{m: "10px 0"}}/>
            <Typography fontWeight="18px" pl="8px">
                {props.content}
            </Typography>
        </Box>
    );
}

export default DetailsLabeledContent;