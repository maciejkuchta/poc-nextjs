/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    HOST: process.env.HOST,
    API_URL: process.env.API_URL
  },
  images: {
    domains: ['localhost'],
  },
}

module.exports = nextConfig
