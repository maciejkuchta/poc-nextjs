interface ITextRecord {
    id: number,
    content: string
}

interface IImage {
    id: number,
    url: string
}

export interface ITechnology {
    id: number,
    name: string,
    text_records: ITextRecord[],
    image: IImage
}

interface IDescription {
    id: number,
    content: string,
    text_records: ITextRecord[]
}

export interface IHomePageData {
    id: number,
    title: string,
    section_subtitle: string,
    main_banner_image: IImage,
    technologies: ITechnology[],
    description: IDescription
}