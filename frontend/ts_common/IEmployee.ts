export interface IEmployee {
    id: number,
    attributes: {
        firstName: string,
        lastName: string,
        employmentDate: string | null,
        position: string,
        subordinates:  {
            data: IEmployee[] | []
        }
    }
}

export interface IEmployeeFlat {
    id: number,
    firstName: string,
    lastName: string,
    employmentDate: string | null,
    position: string,
    isChairperson: boolean,
    avatar: {
        url: string
    } | null,
    subordinates: IEmployeeFlat[] | [] | undefined

}