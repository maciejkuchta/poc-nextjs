export interface IMessage {
    id: number | string,
    attributes: {
        name: string,
        email: string,
        phoneNumber: string,
        topic: string,
        content: string,
        createdAt: string
    }
}