import {regexLiteral} from "@babel/types";

export interface IEvent {
    target: {
        name: string,
        value: string
    }
}

export interface IFormData {
    [key: string]: string,
    name: string,
    phoneNumber: string,
    email: string,
    topic: string,
    content: string
}

interface IValidationItem {
    required?: boolean,
    regex?: {
        value: RegExp,
        errorMsg: string
    }

}

export interface IFormValidation {
    [key: string]: IValidationItem,
    name: IValidationItem,
    phoneNumber: IValidationItem,
    email: IValidationItem,
    topic: IValidationItem,
    content: IValidationItem
}

export interface ISocialMediaLink {
    url: string,
    name: string,
    logo: {
        url: string
    }
}

export interface ICompany {
        id: number | string,
        name: string,
        street: string,
        postcode: string,
        city: string,
        building_number: string,
        nip: string,
        email: string,
        phoneNumber: string
}

export interface IContactPageData {
    id: number | string,
    title: string,
    company: ICompany,
    social_media_links: ISocialMediaLink[]
}