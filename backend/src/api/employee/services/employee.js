'use strict';

/**
 * employee service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::employee.employee', ({strapi}) => ({
    async getSubordinatesRecursively(id) {
       const employee = await strapi.entityService.findOne('api::employee.employee', id, {
         fields: ["firstName", "lastName", "employmentDate", "position"],
         populate: {
           subordinates: {
             fields: ["id"]
           },
           avatar: {
             fields: ["url"]
           }
         }
       });
       if (employee.subordinates.length === 0) return employee;
       else {
         for (let i=0; i<employee.subordinates.length; i++) {
           employee.subordinates[i] = await this.getSubordinatesRecursively(employee.subordinates[i].id);
         }
         return employee;
       }
    }
}));
