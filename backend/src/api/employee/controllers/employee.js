'use strict';

/**
 *  employee controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::employee.employee', ({ strapi }) =>  ({
  async getOfficeStructure(ctx) {
    try {
      const chairpersons = await strapi.entityService.findMany('api::employee.employee', {
        filters: {
          isChairperson: true
        },
        fields: ["id"]
      });
      for (let i = 0; i<chairpersons.length; i++) {
        chairpersons[i] = await strapi.service('api::employee.employee').getSubordinatesRecursively(chairpersons[i].id);
      }

      ctx.body = chairpersons;
    } catch (error) {
      ctx.body = error;
    }
  }

}));
