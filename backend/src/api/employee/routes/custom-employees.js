module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/employees/office-structure',
      handler: 'employee.getOfficeStructure',
    }
  ]
}
