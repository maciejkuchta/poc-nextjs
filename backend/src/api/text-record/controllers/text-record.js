'use strict';

/**
 *  text-record controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::text-record.text-record');
