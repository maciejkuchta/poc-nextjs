'use strict';

/**
 * text-record service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::text-record.text-record');
