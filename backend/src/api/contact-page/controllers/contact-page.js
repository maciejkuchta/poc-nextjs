'use strict';

/**
 *  contact-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::contact-page.contact-page', ({strapi}) => ({
  async getContactPageData(ctx) {
    try {
      ctx.body = await strapi.entityService.findOne('api::contact-page.contact-page', 1, {
        fields: ["title"],
        populate: {
          company: {
            fields: "*"
          },
          social_media_links: {
            fields: ["url", "name"],
            populate: {
              logo: {
                fields: ["url"]
              }
            }
          },
          description: {
            fields: ["content"],
            populate: {
              text_records: {
                fields: ["content"]
              }
            }
          }
        }
      });
    } catch (error) {
      ctx.body = error;
    }
  }
}));
