module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/contact-page/data',
      handler: 'contact-page.getContactPageData',
    }
  ]
}
