'use strict';

/**
 *  home-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::home-page.home-page', ({strapi}) => ({
  async getHomePageData(ctx) {
      try {
        ctx.body = await strapi.entityService.findOne('api::home-page.home-page', 1, {
          fields: ["title", "section_subtitle"],
          populate: {
            main_banner_image: {
              fields: ["url"]
            },
            technologies: {
              fields: ["name"],
              populate: {
                text_records: {
                  fields: ["content"]
                },
                image: {
                  fields: ["url"]
                }
              }
            },
            description: {
              fields: ["content"],
              populate: {
                text_records: {
                  fields: ["content"]
                }
              }
            }
          }
        });
      } catch (error) {
        ctx.body = error;
      }
  }
}));
